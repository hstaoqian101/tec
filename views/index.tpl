<!DOCTYPE html>
<html>
<head>
  <title>TEC</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.staticfile.org/popper.js/1.12.5/umd/popper.min.js"></script>
  <script src="https://cdn.staticfile.org/twitter-bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>The Economist Companion</h1>
  <h5>使用说明</h5> 
  <p>点击上传按钮, 上传 txt 格式文件的 The Economist, 获得文章的词频统计, 选择需要学习的单词, 保存为 csv 格式, 可以导入 Anki 中学习 </p>
</div>
 
<div class="container">
    <form action="/" method="post" enctype="multipart/form-data">
        <label for="exampleFormControlTextarea1">要分析的文本</label>
        <textarea name="textarea" class="form-control" id="exampleFormControlTextarea1" rows="10" placeholder="You can paste any English text here..."></textarea>
        <input type="submit" class="btn btn-primary" value="提交" />
    </form>  
</div>



