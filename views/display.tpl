<!DOCTYPE html>
<html>
<head>
  <title>TEC</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.staticfile.org/popper.js/1.12.5/umd/popper.min.js"></script>
  <script src="https://cdn.staticfile.org/twitter-bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>The Economist Companion</h1>
  <h5>使用说明</h5> 
  <p>点击上传按钮, 上传 txt 格式文件的 The Economist, 获得文章的词频统计, 选择需要学习的单词, 保存为 csv 格式, 可以导入 Anki 中学习 </p>
</div>
 
<div class="container">
    % if data:
    <form method='post' action='/display'>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th scope="row">单词</th>
                            <th scope="row">词义</th>
                            <th scope="row">句子</th>
                            <th scope="row">export</th>
                        </tr>
                    </thead>
                    % for key, value in data.items():
                    <tbody>
                        % if value['is_coca']:
                        <tr class='table-success'>
                            <td>{{ key }}</td>
                            <td>
                              % for meaning in value['meaning']:
                                 {{ meaning }}
                              % end
                            </td>
                            <td>
                            % for sentence in value['sentences']:
                                {{ sentence }}
                            % end
                            </td>
                            <td><input type="checkbox" name="output" value="{{ get('key', 'value') }}" checked="checked"/>&nbsp;</td>
                        </tr>
                        % else:
                        <tr>
                            <td>{{ key }}</td>
                            <td>
                              % for meaning in value['meaning']:
                                 {{ meaning }}
                              % end
                            </td>
                            <td>
                            % for sentence in value['sentences']:
                                {{ sentence }}
                            % end
                            </td>
                            <td><input type="checkbox" name="output" value="{{ get('key', 'value') }}"/>&nbsp;</td>
                        </tr>
                        % end
                    </tbody>
                    % end
                </table>
            </div>
        </div>
        <input type="submit" class="btn btn-success offset-lg-10">
    </form>
    % else:
    <h1> No Data</h1>
% end

</div>

