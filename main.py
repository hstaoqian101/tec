import os
import re
from bottle import run, route, template, request, static_file, response, redirect
from pathlib import Path
from helper import process_data, count_data, find_sentence_with_word, is_coca, generate_csv_file, write_to_file


@route('/')
def index():
    return template('index')

@route('/', method='POST')
def do_index():
    if request.forms.get('textarea'):
        text = request.forms.get('textarea')
        write_to_file(text)
        words = process_data(text=text)
        print(words)
        data = find_sentence_with_word(words=words, text=text)
        return template('display', data=data)


@route('/display', method='post')
def do_display():
    if request.forms.getall('output'):
        words = request.forms.getall('output')
        with open('data.txt') as f:
            text = f.read()
        data = find_sentence_with_word(words=words, text=text)
        generate_csv_file(data)
        root = Path('.') / 'download'
        final_p = root / 'output.csv'
        file_name = final_p.resolve()
        with open(file_name, 'w') as f:
            for word in data:
                f.write(word)
                f.write('\t')
                for sentence in data[word]['sentences']:
                    f.write(sentence)
                    f.write(' | ')
                f.write('\n') 
    return template('download')    


@route('/download/<filename:path>')
def download(filename):
    return static_file(filename, root='download', download=filename)

if os.environ.get('APP_LOCATION') == 'heroku':
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
else:
    run(host='localhost', port=8080, debug=True, reloader=True)