import json
from urllib.parse import urlencode
from urllib.request import urlopen
import re
from sys import argv 
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

def fetch(query_str):
    query = {'q': "".join(query_str)}   # list --> str: "".join(list)
    url = 'https://fanyi.youdao.com/openapi.do?keyfrom=11pegasus11&key=273646050&type=data&doctype=json&version=1.1&' + urlencode(query)
    response = urlopen(url, timeout=3)
    html = response.read().decode('utf-8')
    return html

def parse(html):
    d = json.loads(html)
    try:
        if d.get('errorCode') == 0:
            explains = d.get('basic').get('explains')
            result = str(explains).replace('\'', "").replace('[', "").replace(']', "")  #.replace真好用~
            return list(explains)
        else:
            print('无法翻译!****')
            return list()
    except:
        print('****翻译出错!')      #若无法翻译，则空出来
        return list()

def translate(word):
    return parse(fetch(word))
